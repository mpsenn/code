#!/usr/bin/env python3

import names
import numpy

_dimensions = 2
_average = 0
_stddev = 1000

def neighbor(identifier=None):
	name = identifier or names.get_full_name()
	location = numpy.random.randn(_dimensions) * _stddev + _average
	return [name, *location]

def neighborhood(count):
	for i in range(count):
		yield neighbor()

def read_neighborhood(csvfile):
	import csv, sys
	for row in csv.reader(csvfile):
		# yield the name as a string, all else as floats
		yield [row[0], *map(float, row[1:])]

if __name__ == '__main__':
	import argparse, csv, sys

	parser = argparse.ArgumentParser(description='Generate a number of neighbors, and give them names.')
	parser.add_argument('-d', '--dimensions', type=int, required=False, default=2, help="number of dimensions for each neighbor's address")
	parser.add_argument('neighbors', type=int, help='a number of neighbors to generate')
	args = parser.parse_args()

	count = args.neighbors
	_dimensions = args.dimensions

	writer = csv.writer(sys.stdout)
	[writer.writerow(neighbor()) for _ in range(count)]
