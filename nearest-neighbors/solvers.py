#!/usr/bin/env python3

from functools import partial
from heapq import nsmallest

import numpy as np
from scipy.spatial import cKDTree as KDTree
from scipy.spatial.distance import euclidean

import neighborhood as neighborlib

# Returns the distance between two neighbor arrays. first and second do not include the name
def distance(first, second):
	return euclidean(first, second)

def to_numpy_array(neighborhood):
	names = [n[0] for n in neighborhood]
	locations = [n[1:] for n in neighborhood]
	return (names, np.array(locations),)

class SortSolver():
	def __init__(self, neighborhood):
		self._names, self._locations = to_numpy_array(neighborhood)

	def query(self, index, count):
		me = self._locations[index]
		name = self._names[index]

		distances = [euclidean(me, other) for other in self._locations]

		# Add one for me and remove the first one because that is me
		neighbor_indices = np.argsort(distances)
		neighbor_indices = neighbor_indices[1:(count+1)]

		neighbor_names = [self._names[i] for i in neighbor_indices]
		return neighbor_names


class HeapSolver():
	def __init__(self, neighborhood):
		self._names, self._locations = to_numpy_array(neighborhood)

	def query(self, index, count):
		me = self._locations[index]
		name = self._names[index]
		keyfunc = lambda n_index: distance(me, self._locations[n_index])

		# Add one for me, then remove the first one because that is me
		neighbor_indices = nsmallest(count+1, range(len(self._names)), key=keyfunc)
		neighbor_indices = neighbor_indices[1:]

		neighbor_names = [self._names[i] for i in neighbor_indices]
		return neighbor_names

class TreeSolver:
	def __init__(self, neighborhood):
		self._names, self._locations = to_numpy_array(neighborhood)
		self._neighborhood = KDTree(self._locations)

	# Find the nearest neighbors for a given neighbor. Call prepare() first
	def query(self, index, count):
		me = self._locations[index]
		name = self._names[index]

		# Add one for me, then remove the first one because that is me
		(distances, neighbor_indices) = self._neighborhood.query(me, count+1)
		(distances, neighbor_indices) = (distances[1:], neighbor_indices[1:])

		neighbor_names = [self._names[i] for i in neighbor_indices]
		return neighbor_names

if __name__ == '__main__':
	import sys
	sys.exit("Run with nearest_neighbors.py")
