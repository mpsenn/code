#!/usr/bin/env python3

import neighborhood as neighborlib
import solvers

def solve_with(solver, neighborhood, count):
	size = len(neighborhood)

	for index in range(size):
		name = neighborhood[index][0]
		nearest = solver.query(index, count)
		print("{}\t-> {}".format(name, nearest))

if __name__ == '__main__':
	import argparse, sys

	parser = argparse.ArgumentParser(description='Read in a list of neighbors, and print the nearest neighbors for each.')
	parser.add_argument('--infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
	parser.add_argument('neighbors', type=int, help='number of nearest neighbors to find for each neighbor')
	args = parser.parse_args()

	neighbor_count = args.neighbors
	neighbors = neighborlib.read_neighborhood(args.infile)
	neighborhood = list(neighbors)

	print("TreeSolver")
	solve_with(solvers.TreeSolver(neighborhood), neighborhood, neighbor_count)

	print("HeapSolver")
	solve_with(solvers.HeapSolver(neighborhood), neighborhood, neighbor_count)

	print("SortSolver")
	solve_with(solvers.SortSolver(neighborhood), neighborhood, neighbor_count)
