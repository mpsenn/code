#!/usr/bin/env python3

import timeit

import neighborhood as neighborlib
import solvers

class Scenario:
	def __init__(self, solver, neighborhood, count):
		self.solver = solver
		self.size = len(neighborhood)
		self.count = count

	def __call__(self):
		for index in range(self.size):
			self.solver.query(index, self.count)


def solve(name, scenario):
	results = timeit.repeat(scenario, number=1)
	print(name, results)

if __name__ == '__main__':
	import argparse, sys

	parser = argparse.ArgumentParser(description='Read in a list of neighbors, and print the nearest neighbors for each.')
	parser.add_argument('--infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
	parser.add_argument('neighbors', type=int, help='number of nearest neighbors to find for each neighbor')
	args = parser.parse_args()

	count = args.neighbors
	neighbors = neighborlib.read_neighborhood(args.infile)
	neighborhood = list(neighbors)

	scenario = Scenario(solvers.TreeSolver(neighborhood), neighborhood, count)
	solve("TreeSolver", scenario)

	scenario = Scenario(solvers.HeapSolver(neighborhood), neighborhood, count)
	solve("HeapSolver", scenario)

	scenario = Scenario(solvers.SortSolver(neighborhood), neighborhood, count)
	solve("SortSolver", scenario)
